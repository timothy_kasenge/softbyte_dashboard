import axios from 'axios'
// let url = process.env.NODE_ENV === 'development' ? process.env.API_URL : process.env.API_URL;
// var header_variables = {
//     'Authorization': `${window.localStorage.getItem('token')}`,
//     'Content-Type': 'application/json'
// }

// const HTTP = axios.create({
//     baseURL: `${url}`,
//     headers: header_variables

// })

export const toggleChat = ({
    commit
}) => {
    commit('toggleChat');
}

export const toggleSidebar = ({
    commit
}) => {
    commit('toggleSidebar');
}

export const switchSidebar = ({
    commit
}, value) => {
    commit('switchSidebar', value);
}

export const handleSwipe = ({
    commit
}, e) => {
    commit('handleSwipe', e);
}

export const changeSidebarActive = ({
    commit
}, index) => {
    commit('changeSidebarActive', index);
}