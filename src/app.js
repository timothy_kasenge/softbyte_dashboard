import Vue from 'vue'

import App from '@/components/layout/Main'
import {
    createRouter
} from "@/router"

// external imports
import ex from './external_imports'

import store from './store'

export function createApp() {
    const router = createRouter()

    router.beforeEach((to, from, next) => {
        // to and from are both route objects

        if (to.meta.requiresAuth) {
            const authUser = window.localStorage.getItem('token')

            if (!authUser.length) {
                next({
                    path: '/login'
                })
            } else {
                next()
            }
        } else if (to.meta.checkAccount) {
            const authUser = window.localStorage.getItem('token')
            if (authUser.length) {
                next({
                    path: '/dashboard'
                })
            } else {
                next()
            }
        } else {
            next()
        }
    })

    const app = new Vue({
        router,
        store,
        render: h => h(App)
    })

    return {
        app
    }
}