import Vue from 'vue'


// import orderBy from 'lodash/orderBy';
// Object.defineProperty(Vue.prototype, '$orderBy', {
//   value: orderBy
// });

// import difference from 'lodash/difference';
// Object.defineProperty(Vue.prototype, '$difference', {
//   value: difference
// });

// import intersection from 'lodash/intersection';
// Object.defineProperty(Vue.prototype, '$intersection', {
//   value: intersection
// });

// import filter from 'lodash/filter';
// Object.defineProperty(Vue.prototype, '$filter', {
//   value: filter
// });

// import uniq from 'lodash/uniq';
// Object.defineProperty(Vue.prototype, '$uniq', {
//   value: uniq
// });

// // date picker
// import flatPickr from 'vue-flatpickr-component';
// Vue.component('flat-pickr', flatPickr)

// //font awesome
// // import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
// // import fontawesome from '@fortawesome/fontawesome'
// // // import brands from '@fortawesome/fontawesome-free-brands'
// // // import regular from '@fortawesome/fontawesome-free-solid'

// // // brand icons
// // // import faWhatsappSquare from '@fortawesome/fontawesome-free-brands/faWhatsappSquare'

// // // regular icons
// // import faArrowLeft from '@fortawesome/fontawesome-free-solid/faArrowLeft'
// // import faArrowRight from '@fortawesome/fontawesome-free-solid/faArrowRight'
// // import faCheckCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle'
// // import faCaretDown from '@fortawesome/fontawesome-free-solid/faCaretDown'


// // fontawesome.library.add(faArrowLeft, faArrowRight, faCheckCircle, faCaretDown)
// //     // fontawesome.config = {
// //     //     autoAddCss: false,
// //     // };

// // Vue.component('font-awesome-icon', FontAwesomeIcon)

// //validate
// import VeeValidate from 'vee-validate';
// Vue.use(VeeValidate);

// //moment
// // Vue.use(require('vue-moment'));
// import moment from 'moment';
// Object.defineProperty(Vue.prototype, '$moment', {
//   value: moment
// });


// // modal
// // import VModal from 'vue-js-modal'
// // Vue.use(VModal, {
// //     dialog: true
// // })

// //tel input
// import IntlTelInput from 'intl-tel-input'
// Vue.use(IntlTelInput)

// // slick carousel
// import SlickCarousel from 'slick-carousel'
// Vue.use(SlickCarousel)
// import Slick from 'vue-slick';
// Vue.component('slick', Slick);

// // Gmap
// import * as VueGoogleMaps from 'vue2-google-maps'
// Vue.use(VueGoogleMaps, {
//   load: {
//     key: 'AIzaSyDq6UI1FsVcF8bNrB1yJuZwdeBmEnVcbRM',
//     libraries: 'places, drawing, visualization'
//   }
// })


// // vue carousel 3D
// import Carousel3d from 'vue-carousel-3d';
// Vue.use(Carousel3d);

import BootstrapVue from 'bootstrap-vue';
import VueTouch from 'vue-touch';
import Trend from 'vuetrend';

Vue.use(BootstrapVue);
Vue.use(VueTouch);
Vue.use(Trend);