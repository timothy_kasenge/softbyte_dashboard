import './assets/styles/app'

import {
    createApp
} from './app'

const {
    app
} = createApp()

app.$mount('#app')