import Vue from 'vue'
import Router from 'vue-router'

import Home from "@/components/Home";
import Layout from "@/components/layout/Layout";

Vue.use(Router)

const routes = [{
    path: '/',
    component: Layout,
    children: [{
        path: '',
        name: 'Home',
        component: Home,
    }]

}]

export function createRouter() {
    return new Router({
        mode: 'history',
        routes
    })
}