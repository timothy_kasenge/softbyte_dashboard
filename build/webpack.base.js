const path = require('path')
const HtmlWebPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    entry: {
        app: './src/main.js'
    },
    devtool: "source-map",
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/'
    },
    resolve: {
        alias: {
            '@': path.resolve('./src'),
            'styles': path.resolve('./src/assets/styles'),
            'img': path.resolve('./src/assets/img'),
            'fonts': path.resolve('./src/fonts'),
            'vue': 'vue/dist/vue.common.js',
            'moment': 'moment/src/moment',
            'jquery': 'jquery/dist/jquery.min',
        },
        extensions: ['.js', '.vue', '.scss', '.css']
    },
    devServer: {
        historyApiFallback: true,
    },
    module: {
        rules: [{
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss?$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            }, {
                test: /\.css?$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(png|jpg|gif|ico|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'img/',
                    publicPath: '/'

                }
            },
            {
                test: /\.(woff2?|ttf|otf|eot)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'fonts/',
                    publicPath: '/'
                }

            }
        ],
    },
    plugins: [
        new HtmlWebPlugin({
            title: 'Production',
            inject: true,
            filename: 'index.html',
            template: 'index.html',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true
                    // more options:
                    // https://github.com/kangax/html-minifier#options-quick-reference
            },
            // necessary to consistently work with multiple chunks via CommonsChunkPlugin
            chunksSortMode: 'dependency'
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        }),

    ]


}