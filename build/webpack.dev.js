const base = require('./webpack.base.js')
const merge = require('webpack-merge')
const webpack = require('webpack')

const config = require('../config/dev.config')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


module.exports = merge(base, {
    mode: 'development',
    devServer: {
        compress: true,
        port: 8080,
        overlay: true
    },
    plugins: [
        new webpack.DefinePlugin({
            'process': {
                env: config
            }
        }),
        new BundleAnalyzerPlugin({
            analyzeMode: 'static'
        })

    ]


})